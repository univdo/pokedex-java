package com.nmeo;

import io.javalin.Javalin;

import java.util.HashSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.dto.RespondListPokemonDTO;
import com.nmeo.models.AllPokemons;
import com.nmeo.models.Elements;
import com.nmeo.models.Pokemon;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");
        AllPokemons allPokemons = new AllPokemons();

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            try {
                Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
                allPokemons.addPokemon(pokemon);
                ctx.status(200);
            }
            catch (Exception e) {
                ctx.status(400);
            }
        })
        .get("/api/searchByName", ctx -> {
            try {
                String nameToSearch = ctx.queryParam("name");
                if (nameToSearch == null) {
                    ctx.status(400);
                } else {
                    HashSet<Pokemon> pokemons = allPokemons.searchByName(nameToSearch);
                    RespondListPokemonDTO response = new RespondListPokemonDTO(pokemons);
                    ctx.json(response);
                    ctx.status(200);
                }
                
            } catch (Exception e) {
                ctx.status(400);
            }
            

        })
        .post("/api/modify", ctx -> {
            try {
                ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);
                request.verifyRequest();
                if (allPokemons.updatePokemon(request)) {
                    ctx.status(200);
                } else {
                     ctx.status(404);
                }
            } catch (Exception e) {
                 ctx.status(400);
            }
            
        })
        .get("/api/searchByType", ctx -> {
            try {
                String typeToSearch = ctx.queryParam("type");
                if (typeToSearch == null) {
                    ctx.status(400);
                } else {
                    Elements element = Elements.valueOf(typeToSearch);
                    HashSet<Pokemon> pokemons = allPokemons.searchByType(element);
                    RespondListPokemonDTO response = new RespondListPokemonDTO(pokemons);
                    ctx.json(response);
                    ctx.status(200);
                }
            } catch (Exception e) {
                ctx.status(400);
            }
        })
        .start(port);
    }
}