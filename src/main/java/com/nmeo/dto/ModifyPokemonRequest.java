package com.nmeo.dto;

import com.nmeo.models.Pokemon;

public class ModifyPokemonRequest extends Pokemon{
    


    public ModifyPokemonRequest() {}


    public void verifyRequest() {
        if (this.pokemonName == null || this.pokemonName == "") {
            throw new RuntimeException("Wrong name format.");
        }
    }

}
