package com.nmeo.dto;

import java.util.HashSet;

import com.nmeo.models.Pokemon;

public class RespondListPokemonDTO {

    public HashSet<Pokemon> result;

    
    public RespondListPokemonDTO(HashSet<Pokemon> pokemons) {
        this.result = pokemons;
    }
    
}
