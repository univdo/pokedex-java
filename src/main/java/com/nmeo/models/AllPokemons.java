package com.nmeo.models;

import java.util.HashSet;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import java.util.Optional;
import com.nmeo.dto.ModifyPokemonRequest;

public class AllPokemons {

    private HashSet<Pokemon> pokemons;
    private final ReadWriteLock readWriteLock;

    public AllPokemons() {
        this.pokemons = new HashSet<Pokemon>();
        this.readWriteLock = new ReentrantReadWriteLock();
    }


    public HashSet<Pokemon> getPokemons() {
        return this.pokemons;
    }

    public void addPokemon(Pokemon pokemon) {
        this.readWriteLock.writeLock().lock();
        if (this.pokemons.contains(pokemon)) {
            this.readWriteLock.writeLock().unlock();
            throw new RuntimeException("There already exists a pokemon with this name.");
        }
        this.pokemons.add(pokemon);
        this.readWriteLock.writeLock().unlock();
    }

    public HashSet<Pokemon> searchByName(String name) {
        HashSet<Pokemon> pokes = new HashSet<>();
        for (Pokemon pokemon : this.pokemons) {
            if (pokemon.pokemonName.contains(name)) {
                pokes.add(pokemon);
            }
        }
        return pokes;
    }

    public HashSet<Pokemon> searchByType(Elements type) {
        HashSet<Pokemon> pokes = new HashSet<>();
        for (Pokemon pokemon : this.pokemons) {
            if (pokemon.type.equals(type)) {
                pokes.add(pokemon);
            }
        }
        return pokes;
    }

    public boolean updatePokemon(ModifyPokemonRequest modifyPokemon) {
        this.readWriteLock.writeLock().lock();
        Optional<Pokemon> poke = this.pokemons.stream().filter(p -> p.compareTo(modifyPokemon) == 0).findFirst();
        if(!poke.isPresent()) {
            this.readWriteLock.writeLock().unlock();
            return false;
        }
        Pokemon pokemon = poke.get();
        if (modifyPokemon.type != null) {
            pokemon.type = modifyPokemon.type;
        }
        if (modifyPokemon.lifePoints != 0) {
            pokemon.lifePoints = modifyPokemon.lifePoints;
        }
        if (modifyPokemon.powers != null) {
            for (Power pow : modifyPokemon.powers) {
                if (!pokemon.powers.contains(pow)) {
                    pokemon.powers.add(pow);
                }
            }
            
        }
        this.readWriteLock.writeLock().unlock();
        return true;
    }

}
