package com.nmeo.models;

public enum Elements {
        NEUTRAL,
        FIRE,
        WATER,
        GRASS,
        ELECTRIC,
        ICE,
        FIGHTING,
        POISON,
        GROUND;
};

