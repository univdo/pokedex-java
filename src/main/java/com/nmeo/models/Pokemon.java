package com.nmeo.models;

import java.util.List;

public class Pokemon implements Comparable<Pokemon> {

    public String pokemonName;
    public Elements type;
    public int lifePoints;
    public List<Power> powers;


    public Pokemon() {}

    public Pokemon(String pokemonName, Elements type, int lifePoints, List<Power> powers) {
        this.pokemonName = pokemonName;
        this.type = type;
        this.lifePoints = lifePoints;
        this.powers = powers;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Pokemon) {
            Pokemon poke = (Pokemon) o;
            return this.pokemonName.equals(poke.pokemonName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.pokemonName.hashCode();
    }

    @Override 
    public int compareTo(Pokemon poke) {
        return this.pokemonName.equals(poke.pokemonName) ? 0 : 1;
    }
}