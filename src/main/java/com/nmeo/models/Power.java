package com.nmeo.models;


public class Power {

    public String powerName;
    public Elements damageType;
    public int damage;


    public Power() {}


    public Power(String powerName, Elements damageType, int damage) {
        this.powerName = powerName;
        this.damageType = damageType;
        this.damage = damage;
    }
}